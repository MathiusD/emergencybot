default_target: deploy

start:
	@echo "Launch Bot"
	@python3 bot.py

launch: start

deploy: rebuild launch

build:
	@sh build.sh

clear_log:
	@rm logs/*.log

clear:
	@echo "Clear Repo"
	@rm -rf own own_install.py

rebuild: clear update build

update:
	@git pull

docker_update: update
	@docker-compose build

docker_force_update: update
	@docker-compose build --no-cache

docker_up: docker_update docker_start

docker_start:
	@docker-compose up -d

docker_down:
	@docker-compose down

docker_restart: docker_down docker_start

docker_reload: docker_force_update docker_restart