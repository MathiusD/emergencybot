from discord.ext import commands
from discord import User, Member, TextChannel, Emoji, Role
from own import own_discord
from utils import events, GameEvent, Personna, Ejection
from aiohttp import ClientSession
import random
import re
import asyncio


class AmongUs(commands.Cog):

    @commands.command(description="Command for start vote.")
    async def vote(self, ctx: commands.Context):
        game = GameEvent(ctx)
        await game.launch()

    @commands.command(description="Command for start random event.")
    async def sabotage(self, ctx: commands.Context):
        await ctx.typing()
        nbMembers = len(await own_discord.fetchMembers(ctx.channel)) - 1 if isinstance(ctx.channel, TextChannel) else 1
        eventsMembers = []
        for key in events.keys():
            if key <= nbMembers:
                for event in events[key]:
                    eventsMembers.append(event)
        if len(eventsMembers) > 0:
            eventId = random.randint(0, len(eventsMembers) - 1)
            event = eventsMembers[eventId](ctx)
            await event.launch()
        else:
            await own_discord.reponse_send(ctx, "No events are available at least %i members" % nbMembers)

    @commands.command(description="Command for eject anyone.\nCoolDown of 15s.")
    @commands.cooldown(rate=1, per=15, type=commands.BucketType.user)
    async def eject(self, ctx: commands.Context, *, data=None):
        await ctx.typing()
        if not (isinstance(data, (str, User, Member, TextChannel, Role, Emoji, Personna)) or data is None) is not False:
            raise commands.UserInputError()
        if data is not None and isinstance(data, str):
            regexUser = "(<@(!)?[0-9]+>)+"
            regexEmoji = "(<(a)?:.+:[0-9]+>)+"
            regexChan = "(<#[0-9]+>)+"
            regexRole = "(<@&[0-9]+>)+"
            regexUri = "(<)?https://.*(>)?"
            userInData = True if re.match("\n", data) else False
            cutBase = data.split(" ")
            cut = []
            for cutRaw in cutBase:
                for cutInRaw in cutRaw.split("\n"):
                    cut.append(cutInRaw)
            for raw in cut:
                if re.match("%s|%s|%s|%s|%s|\n" % (regexUser, regexEmoji, regexChan, regexRole, regexUri), raw) and userInData is False:
                    userInData = True
            if userInData is False:
                cut = data.split("\n")
            if userInData is True:
                data = None
                for raw in cut:
                    dat = None
                    if re.match(regexUser, raw):
                        idRaw = raw[3:-1] if raw[2] == '!' else raw[2:-1]
                        user = await ctx.bot.fetch_user(int(idRaw))
                        if user is not None:
                            dat = user
                        else:
                            await own_discord.reponse_send(ctx, "User with id %s Not Found." % idRaw)
                    elif re.match(regexEmoji, raw):
                        cutRaw = raw[1:-1].split(":")
                        idRaw = cutRaw[len(cutRaw) - 1]
                        emoji = ctx.bot.get_emoji(int(idRaw))
                        if emoji is not None:
                            dat = emoji
                        else:
                            await self.eject(ctx, data=raw[1:-(len(idRaw)+1)])
                    elif ctx.guild is not None and re.match(regexChan, raw):
                        idRaw = raw[2:-1]
                        chan = ctx.guild.get_channel(int(idRaw))
                        if chan is not None:
                            dat = chan
                        else:
                            await own_discord.reponse_send(ctx, "Chan with id %s Not Found." % idRaw)
                    elif ctx.guild is not None and re.match(regexRole, raw):
                        idRaw = raw[3:-1]
                        role = ctx.guild.get_role(int(idRaw))
                        if role is not None:
                            dat = role
                        else:
                            await own_discord.reponse_send(ctx, "Role with id %s Not Found." % idRaw)
                    elif re.match(regexUri, raw):
                        raw = raw[1:] if raw.startswith("<") else raw
                        raw = raw[:-1] if raw.endswith(">") else raw
                        found = False
                        async with ClientSession() as session:
                            async with session.get(raw) as response:
                                httpData = await response.content.read()
                                httpData = str(httpData, "utf-8")
                                match = re.search(
                                    ".*<title.*>(.*)</title>.*", httpData)
                                if match is not None:
                                    index = match.regs[1]
                                    found = True
                                    await self.eject(ctx, data=httpData[index[0]:index[1]])
                        if found is False:
                            dat = raw[8:] if raw.startswith(
                                "https://") else raw[7:]
                    if dat is None:
                        dat = raw
                    if len(cut) > 1:
                        if data is None:
                            data = []
                        data.append(dat)
                    else:
                        data = dat
        if isinstance(data, list):
            asyncio.run_coroutine_threadsafe(
                Ejection.sendManyWithIdentification(ctx, data), ctx.bot.loop)
        else:
            asyncio.run_coroutine_threadsafe(
                Ejection.sendWithIdentification(ctx, data), ctx.bot.loop)
