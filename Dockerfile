FROM python:3.12

WORKDIR /usr/docker/emergencybot

COPY . .

RUN make rebuild

CMD ["make", "start"]