import time
import logging
import discord
import traceback
from cogs import AmongUs
from discord.ext import commands
from config import settings
from own import own_discord

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s',
                    filename='logs/%s.log' % (str(time.time())), level=settings.DISCORD_LOG)

intent = discord.Intents.default()

intent.members = True


def determine_prefix(bot: commands.AutoShardedBot, message: discord.Message):
    return commands.when_mentioned_or(settings.DISCORD_PREFIX)(bot, message)


class MyBot(commands.AutoShardedBot):
    async def setup_hook(self):
        await self.add_cog(AmongUs())

        dblpy = None
        if settings.DBL_TOKEN is not None:
            import dbl
            dblpy = dbl.DBLClient(self, settings.DBL_TOKEN)

        async def postGuild():
            if dblpy:
                await dblpy.post_guild_count()

        @bot.event
        async def on_guild_join(guild: discord.Guild):
            await postGuild()

        @bot.event
        async def on_guild_remove(guild: discord.Guild):
            await postGuild()

        @bot.event
        async def on_ready():
            act = discord.Game(settings.DISCORD_HELP)
            await self.change_presence(activity=act)
            await postGuild()
            logging.info("Bot Launch.")
            print("Bot Up !")

        async def commandError(ctx: commands.Context, exc: commands.CommandError):
            log = True
            if ctx.command:
                if isinstance(exc, commands.UserInputError):
                    log = False
                    await own_discord.reponse_send(ctx, "Bad Usage of this command.")
                elif isinstance(exc, commands.MissingPermissions):
                    log = False
                    await own_discord.reponse_send(ctx, "Missing Permissions for perform this command.")
                elif isinstance(exc, commands.CommandOnCooldown):
                    log = False
                    await own_discord.reponse_send(ctx, exc.__str__())
                else:
                    app = await bot.application_info()
                    await own_discord.reponse_send(ctx, "Error as occured please contact <@%i> (%s#%s)." % (app.owner.id, app.owner.name, app.owner.discriminator))
            if log:
                if not isinstance(exc, commands.CommandNotFound):
                    logs = traceback.format_exception(
                        type(exc), exc, exc.__traceback__)
                    location = "For User %s#%s(%i)" % (
                        ctx.author.name, ctx.author.discriminator, ctx.author.id)
                    if ctx.command:
                        location = "In use %s %s" % (
                            ctx.command.name, location)
                    if ctx.guild:
                        location = "%s In Guild \"%s\"(%i)" % (
                            location, ctx.guild.name, ctx.guild.id)
                    logs.append(location)
                    logs.append("Call : %s" % ctx.message.content)
                    for line in logs:
                        print(line)
                        logging.warning(line)
        if dblpy:
            @bot.command()
            async def invite(ctx: commands.Context):
                await ctx.typing()
                data = await dblpy.get_bot_info()
                await ctx.send(data["invite"] if "invite" in data.keys() else "No renseigned")

        self.on_command_error = commandError


bot = MyBot(command_prefix=determine_prefix,
            case_insensitive=True, intents=intent)

bot.run(settings.DISCORD_TOKEN)
