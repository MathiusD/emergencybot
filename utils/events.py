from discord.ext import commands
from discord import Message
from own import own_discord
from enum import Enum
import abc
import datetime
import asyncio
import random


class State(Enum):
    CANCELED = -2
    FAILED = -1
    IN_PROGRESS = 0
    SUCESS = 1


class BaseEvent:

    message = "Basic Event"
    color = None
    refreshTime = 1
    description = None
    updateRequired = False

    @classmethod
    def minUser(cls):
        return 1

    def __init__(self, ctx: commands.Context, msg: Message = None):
        self.finish = False
        self.ctx = ctx
        self.msg = msg
        self.started = None

    @property
    def content(self):
        return "%s\n\nExplanation :\n%s" % (self.description, self._explanation())

    @abc.abstractmethod
    def _explanation(self):
        pass

    @property
    def explanation(self):
        return self._explanation()

    @abc.abstractmethod
    async def _state(self):
        pass

    @property
    def embed(self):
        return own_discord.embed_create(self.content, name=self.message, colour=self.color)

    async def send(self):
        self.msg = await own_discord.embed_send(self.ctx, self.embed, react_unlocked=True)

    async def update(self, embed: list = None):
        await own_discord.embed_update(self.ctx, self.msg, self.embed if embed is None else embed, react_unlocked=True)

    @property
    async def state(self):
        return await self._state()

    async def launch(self):
        if self.msg is None:
            await self.send()
        else:
            await self.update()
        while await self.state == State.IN_PROGRESS:
            if self.updateRequired is True:
                await self.update()
            await asyncio.sleep(self.refreshTime)
        await self.end()

    async def updateEnd(self):
        state = await self.state
        embed = own_discord.embed_create(self.content, name="%s (%s)" % (
            self.message, state.name), colour=self.color)
        await self.update(embed)

    async def end(self):
        self.finish = True
        await self.updateEnd()


class TimeEvent(BaseEvent):

    message = "Base Time Event"

    async def launch(self):
        self.started = datetime.datetime.now().timestamp()
        await super().launch()

    @property
    def timeElapsed(self):
        timestamp = datetime.datetime.now().timestamp()
        return timestamp - self.started


class Arg:

    def __init__(self, name: str, necessary: bool, out: bool):
        self.name = name
        self.necessary = necessary
        self.out = out


class ArgCond:

    @abc.abstractmethod
    def check(self, event: BaseEvent):
        pass


class TrueArgCond(ArgCond):

    def check(self, event: BaseEvent):
        return True


defaultArgCond = TrueArgCond()


class ArgCondValue(ArgCond):

    def __init__(self, name: str, value: object):
        self.name = name
        self.value = value

    def check(self, event: BaseEvent):
        if hasattr(event, self.name):
            if getattr(event, self.name) == value:
                return True
        return False


class ArgCondLen(ArgCondValue):

    def check(self, event: BaseEvent):
        if hasattr(event, self.name):
            if len(getattr(event, self.name)) == value:
                return True
        return False


class PhaseCond:

    def __init__(self, args: list = []):
        self.args = args


class Phase:

    def __init__(self, classAssociated, args: list = [], cond: PhaseCond = None):
        self.classAssociated = classAssociated
        self.args = args
        self.cond = cond if cond is not None else defaultArgCond


class PhasedEvent(BaseEvent):

    phases = []
    message = "Base Phased Event"

    async def launch(self):
        for phase in self.phases:
            execPhase = True
            while (execPhase is True):
                phaseInstance = phase.classAssociated(self.ctx, self.msg)
                for arg in phase.args:
                    if arg.necessary:
                        setattr(phaseInstance, arg.name,
                                getattr(self, arg.name))
                await phaseInstance.launch()
                if await phaseInstance.state != State.SUCESS:
                    return None
                else:
                    for arg in phase.args:
                        if arg.out:
                            setattr(self, arg.name, getattr(
                                phaseInstance, arg.name))
                    self.msg = phaseInstance.msg
                    for react in self.msg.reactions:
                        if react.me is True:
                            await self.msg.remove_reaction(react.emoji, self.ctx.bot.user)
                    if phase.cond.check(self) is True:
                        execPhase = False
        await self.end()

    async def end(self):
        self.finish = True

    async def _state(self):
        return State.IN_PROGRESS if self.finish is False else State.SUCESS


class LimitedTimeEvent(TimeEvent):

    message = "Base Limited Time Event"
    timeout = 30
    updateRequired = True

    async def _state(self):
        return State.IN_PROGRESS if self.isTimeout is False else State.FAILED

    @property
    def timeStr(self):
        return "(%i seconds remaining)" % (self.timeRemaining if self.started is not None else self.timeout)

    def _explanation(self):
        return "You do perform action before timeout %s." % self.timeStr

    @property
    def timeRemaining(self):
        timeElapsed = self.timeElapsed
        return 0 if timeElapsed > self.timeout else self.timeout - timeElapsed

    @property
    def isTimeout(self):
        return False if self.timeRemaining > 0 else True


class CancelableTimeEvent(TimeEvent):

    message = "Base Cancelable Time Event"
    maxTime = 30

    async def _state(self):
        return State.IN_PROGRESS if self.isCanceled is False else State.CANCELED

    @property
    def timeRemainingForCanceled(self):
        timeElapsed = self.timeElapsed
        return 0 if timeElapsed > self.maxTime else self.maxTime - timeElapsed

    @property
    def isCanceled(self):
        return False if self.timeRemainingForCanceled > 0 else True


class EntryEvent(BaseEvent):

    message = "Base Entry Event"
    entry = "Example"
    ownerOnly = True

    def _explanation(self):
        return "%s must enter the expected response related to the event (here: %s)." % ("You" if self.ownerOnly is True else "Anyone", self.entry)

    async def _state(self):
        history = []
        async for history_entry in self.ctx.channel.history(after=self.msg.created_at, oldest_first=True):
            history.append(history_entry)
        for msg in history:
            if (self.ownerOnly is True and msg.author.id == self.ctx.author.id) or self.ownerOnly is False:
                msgContent = own_discord.extractValueFromPingValue(
                    self.ctx, msg)
                try:
                    temp = self.entry.__class__(msgContent)
                except ValueError:
                    temp = None
                if temp == self.entry:
                    return State.SUCESS
        return State.IN_PROGRESS


class IntEntryEvent(EntryEvent):

    message = "Base Int Entry Event"
    entry = 0

    @classmethod
    def getDigit(cls, number: int):
        if number < 10:
            out = [number]
        else:
            out = []
            for num in cls.getDigit(number // 10):
                out.append(num)
            out.append(number % 10)
        return out


class HiddenIntEntryEvent(IntEntryEvent):

    message = "Base Hidden Int Entry Event"
    updateRequired = True

    def _explanation(self):
        explanation = "%s must enter the expected response related to the event.\n" % (
            "You" if self.ownerOnly is True else "Anyone")
        explanation = "%sCode:\n" % (explanation)
        for num in self.getDigit(self.entry):
            explanation = "%s||%i||" % (explanation, num)
        return explanation


class RandomEntryEvent(EntryEvent):

    message = "Base Random Entry Event"

    @abc.abstractmethod
    def defineEntry(self):
        pass

    async def launch(self):
        self.defineEntry()
        await super().launch()


class RandomIntEntryEvent(IntEntryEvent, RandomEntryEvent):

    message = "Base Random Int Entry Event"
    nbDigit = 5
    minValue = 0
    maxValue = 9

    def defineEntry(self):
        for indice in range(self.nbDigit):
            numerator = pow(10, indice)
            self.entry += random.randint(self.minValue,
                                         self.maxValue) * numerator


class HiddenRandomIntEntryEvent(RandomIntEntryEvent, HiddenIntEntryEvent):

    message = "Base Hidden Random Int Entry Event"
    updateRequired = True


class PushEvent(BaseEvent):

    message = "Base Push Event"
    pushEmojis = [
        "🤚",
        "✋"
    ]
    pushByUser = 1
    pushByEmoji = 1
    onlyRequestUser = False

    @classmethod
    def minUser(cls):
        rawValue = (len(cls.pushEmojis) / cls.pushByUser) * cls.pushByEmoji
        return int(rawValue) + 1 if rawValue != int(rawValue) else int(rawValue)

    def _explanation(self):
        descri = "You do interact with Reaction.\n"
        descri = "%sFor each reaction %i simultaneously interaction are required for clear Event.\n" % (
            descri, self.pushByEmoji)
        return "%sEach user can interact with %s reaction without this his actions will not be taken into account." % (descri, self.pushByUser)

    async def _getEmojis(self):
        self.msg = await self.msg.channel.fetch_message(self.msg.id)
        data = {}
        for reaction in self.msg.reactions:
            if reaction.emoji in self.pushEmojis:
                users = []
                async for user in reaction.users():
                    users.append(user)
                if self.ctx.bot.user in users:
                    users.remove(self.ctx.bot.user)
                if self.onlyRequestUser is True:
                    data[reaction.emoji] = [
                        self.ctx.author] if self.ctx.author in users else []
                else:
                    data[reaction.emoji] = users
        return data

    async def _comptEmoji(self):
        data = await self._getEmojis()
        count = {}
        for reaction in data.keys():
            count[reaction] = 0
        for usersKey in data:
            users = data[usersKey]
            for user in users:
                push = 1
                for otherUsersKey in data:
                    otherUsers = data[otherUsersKey]
                    if usersKey != otherUsersKey and user in otherUsers:
                        push += 1
                if push <= self.pushByUser or self.onlyRequestUser is True:
                    count[usersKey] += 1
        return count

    async def _state(self):
        count = await self._comptEmoji()
        confirm = 0
        for comptKey in count:
            compt = count[comptKey]
            if compt >= self.pushByEmoji or (self.onlyRequestUser is True and compt == 1):
                confirm += 1
        if confirm == len(self.pushEmojis):
            return State.SUCESS
        else:
            return State.IN_PROGRESS

    async def addEmojis(self):
        for emoji in self.pushEmojis:
            await self.msg.add_reaction(emoji)

    async def update(self, embed: list = None):
        await super().update(embed)
        await self.addEmojis()

    async def send(self):
        await super().send()
        await self.addEmojis()


class PushLimitedTimeEvent(PushEvent, LimitedTimeEvent):

    message = "Base Push Limited Time Event"
    updateRequired = True

    @classmethod
    def minUser(cls):
        return PushEvent.minUser()

    def _explanation(self):
        rawExplanation = PushEvent._explanation(self)
        return "You do interact with Reaction before timeout %s.\n%s" % (
            self.timeStr,
            rawExplanation[len(rawExplanation.split("\n")[0]):]
        )

    async def _state(self):
        return State.FAILED if await LimitedTimeEvent._state(self) is State.FAILED else await PushEvent._state(self)

    async def launch(self):
        await LimitedTimeEvent.launch(self)


class PushCancelableTimeEvent(PushEvent, CancelableTimeEvent):

    message = "Base Cancelable Limited Time Event"

    @classmethod
    def minUser(cls):
        return PushEvent.minUser()

    def _explanation(self):
        rawExplanation = PushEvent._explanation(self)
        return "You do interact with Reaction before timeout %s.\n%s" % (
            self.timeStr,
            rawExplanation[len(rawExplanation.split("\n")[0]):]
        )

    async def _state(self):
        return State.CANCELED if await CancelableTimeEvent._state(self) is State.CANCELED else await PushEvent._state(self)

    async def launch(self):
        await CancelableTimeEvent.launch(self)


class HiddenRandomIntEntryLimitedTimeEvent(LimitedTimeEvent, HiddenRandomIntEntryEvent):

    message = "Base Hidden Random Int Entry Limited Time Event"

    def _explanation(self):
        rawExplanation = HiddenIntEntryEvent._explanation(self)
        return "You do interact with Reaction before timeout %s.\n%s" % (
            self.timeStr,
            rawExplanation
        )

    async def _state(self):
        return State.FAILED if await LimitedTimeEvent._state(self) is State.FAILED else await RandomIntEntryEvent._state(self)

    async def launch(self):
        self.defineEntry()
        await LimitedTimeEvent.launch(self)


class ReactorEvent(PushLimitedTimeEvent):

    message = "Reactor Meltdown"
    description = "The reactor's going to collapse. You have to purge it to prevent that !"


class SeismicEvent(PushLimitedTimeEvent):

    message = "Overheating of Seismic Stabilizers"
    description = "The seismic stabilizers are overheating, if nothing is done the base will be completely leveled !"


class OxygenEvent(HiddenRandomIntEntryLimitedTimeEvent):

    message = "Oxygen leakage"
    description = "The oxygen reserve is broken, we’re all risking our lives, you need to fix it as soon as possible !"


class CommunicationEvent(PushLimitedTimeEvent):

    message = "Communications are saboted"
    pushEmojis = [
        "📻"
    ]
    pushByEmoji = 3
    description = "You're not receiving anything. Comms have been cut. This is dangerous, I must fix it as soon as possible."


class ElectricalEvent(PushLimitedTimeEvent):

    message = "Electrical cut-off"
    pushEmojis = [
        "🔧",
        "🔩"
    ]
    description = "The base just switched over the emergency lighting that just dropped in its turn. This could be serious if the food stays in this condition for too long."


class SealedDoorEvent(HiddenRandomIntEntryLimitedTimeEvent):

    message = "Sealed door"
    description = "The doors just closed unexpectedly and you need to get out of this room as soon as possible before something happens."


raw_events = [
    ReactorEvent,
    SeismicEvent,
    OxygenEvent,
    CommunicationEvent,
    ElectricalEvent,
    SealedDoorEvent
]
events = {}
for raw in raw_events:
    value = raw.minUser()
    if value in events.keys():
        events[value].append(raw)
    else:
        events[value] = [raw]
