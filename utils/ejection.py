from discord import Color, DMChannel, User, Member, Emoji, TextChannel, Role, File, NotFound, Status
from .crew import Personna, StatePersonna, impostor, crewmate
from discord.ext import commands
from own import own_discord
from PIL import Image, ImageFont, ImageDraw
import random
import os
import uuid
import datetime
import emoji
import requests
import cairosvg
import numpy
import asyncio


class Ejection:

    minConst = 20

    def __init__(self, name: str = None, color: Color = None, imgDataPath: str = None, areChanged: bool = False, state: StatePersonna = None):
        self.name = name if name is not None else "No One"
        self.dataDefined = True if name is not None else False
        self.msg = "%s was ejected" % self.name
        self.color = color if color is not None else Color.from_rgb(
            random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        if self.color.r < self.minConst and self.color.g < self.minConst and self.color.b < self.minConst:
            self.color = Color.from_rgb(
                self.minConst, self.minConst, self.minConst)
        self.imgDataPath = imgDataPath
        self.areChanged = areChanged
        self.state = state
        if self.state is None:
            if random.randint(0, 2) > 0:
                self.state = impostor if random.randint(
                    0, 1) == 0 else crewmate
        if self.state is not None:
            self.impostorMsg = "%s was %sThe Impostor." % (
                self.name, "" if self.state == impostor else "not ")
        else:
            self.impostorMsg = None

    def createMsg(self):
        return "%s\n%s" % (
            "%s\n%s" % (
                self.msg, self.impostorMsg) if self.impostorMsg is not None else self.msg,
            "Sorry but the crew didn’t think it was good to eject me (even if I’m the impostor)." if self.areChanged is True else ""
        )

    def pasteCaracter(self, img: Image.Image):
        if self.dataDefined is True:
            colorPath = "static/img/carac%s.png" % (
                "Transpa" if self.imgDataPath is not None else "")
            cosmo = Image.open(colorPath)
            oldPrinc = (255, 255, 255, 255)
            oldShad = (98, 98, 98, 255)
            newPrinc = (self.color.r, self.color.g, self.color.b, 255)
            shadRatio = 0.8
            newShad = (int(self.color.r * shadRatio), int(self.color.g *
                       shadRatio), int(self.color.b * shadRatio), 255)
            data = numpy.array(cosmo)
            data[(data == oldPrinc).all(axis=-1)] = newPrinc
            data[(data == oldShad).all(axis=-1)] = newShad
            cosmo = Image.fromarray(data)
            if self.imgDataPath:
                tempData = Image.new('RGBA', cosmo.size, (0, 0, 0, 0))
                imgData = Image.open(self.imgDataPath)
                size = 250, 250
                imgData = imgData.resize(size, Image.ANTIALIAS)
                offset = ((cosmo.width - (imgData.width + 150)), 200)
                tempData.paste(imgData, offset)
                tempData.paste(cosmo, (0, 0), cosmo)
                cosmo = tempData
                os.remove(self.imgDataPath)
            rotation = random.randint(1, 360)
            cosmo = cosmo.rotate(rotation, expand=True)
            ratio = random.randint(252, 512)
            size = ratio, ratio
            cosmo.thumbnail(size, Image.ANTIALIAS)
            widthCosmo, heigthCosmo = cosmo.size
            widthRandomCosmo = random.randint(- (widthCosmo // 2),
                                              img.width - (widthCosmo // 2))
            heigthRandomCosmo = random.randint(
                - (heigthCosmo // 2), img.height - (heigthCosmo // 2))
            offset = (widthRandomCosmo, heigthRandomCosmo)
            img.paste(cosmo, offset, cosmo)
        return img

    def pasteTxt(self, img: Image.Image):
        font = ImageFont.truetype('static/fonts/default.ttf', 85)
        draw = ImageDraw.Draw(img)
        widthMsg, heightMsg = draw.textsize(self.msg, font=font)
        heightImpostorMsg = 0
        if self.impostorMsg is not None:
            widthImpostorMsg, heightImpostorMsg = draw.textsize(
                self.impostorMsg, font=font)
        draw.text(((img.width - widthMsg)/2, (img.height - heightMsg)/2 -
                  (heightImpostorMsg)), self.msg, font=font, fill=(255, 255, 255))
        if self.impostorMsg is not None:
            draw.text(((img.width - widthImpostorMsg)/2, (img.height - heightImpostorMsg) /
                      2 + (heightMsg)), self.impostorMsg, font=font, fill=(255, 255, 255))
        return img

    @staticmethod
    def imgBack():
        return Image.open("static/img/default.png")

    @classmethod
    def saveImg(cls, img: Image.Image):
        path = cls.defineUniquePath()
        img.save(path)
        return path

    def createImg(self):
        img = self.imgBack()
        img = self.pasteCaracter(img)
        img = self.pasteTxt(img)
        return self.saveImg(img)

    @classmethod
    async def imgForMany(cls, ejecteds: list):
        img = cls.imgBack()
        for ejected in ejecteds:
            if isinstance(ejected, cls):
                img = ejected.pasteCaracter(img)
                await asyncio.sleep(0.01)
        return cls.saveImg(img)

    @classmethod
    def txtForMany(cls, ejecteds: list):
        txt = None
        for ejected in ejecteds:
            if isinstance(ejected, cls):
                if ejected.areChanged is False:
                    txt = ejected.name if txt is None else "%s, %s" % (
                        txt, ejected.name)
        return "%s were ejected." % txt

    @classmethod
    async def sendForMany(cls, ctx: commands.Context, ejecteds: list):
        msg = await own_discord.reponse_send(ctx, "Processing...")
        path = await cls.imgForMany(ejecteds)
        txt = cls.txtForMany(ejecteds)
        await msg.delete()
        await own_discord.reponse_send(
            ctx,
            txt,
            fil=cls.discordFileGen(path, "%s.png" % txt)
        )
        os.remove(path)

    @classmethod
    async def sendManyWithIdentification(cls, ctx: commands.Context, data: list):
        await cls.sendForMany(ctx, await cls.identifyEjections(ctx, data))

    @staticmethod
    def discordFileGen(path: str, name: str = None):
        name = path if name is None else name
        return File(path, name)

    def discordFile(self, path: str):
        return self.discordFileGen(path, "%s.png" % self.msg)

    async def send(self, ctx: commands.Context):
        path = self.createImg()
        await own_discord.reponse_send(
            ctx,
            self.createMsg(),
            fil=self.discordFile(path)
        )
        os.remove(path)

    @staticmethod
    def defineUniquePath(extension: str = "png"):
        return "static/img/%s.%s" % (uuid.uuid3(uuid.NAMESPACE_DNS, '%s' % (datetime.datetime.now())).__str__(), extension)

    @classmethod
    async def sendWithIdentification(cls, ctx: commands.Context, data):
        msg = await own_discord.reponse_send(ctx, "Identify...")
        obj = await cls.identifyEjection(ctx, data)
        await msg.delete()
        if isinstance(obj, list):
            await cls.sendForMany(ctx, obj)
        else:
            await obj.send(ctx)

    @classmethod
    async def identifyEjections(cls, ctx: commands.Context, data):
        ejecteds = []
        for dat in data:
            identify = await cls.identifyEjection(ctx, dat)
            if isinstance(identify, list):
                for ident in identify:
                    ejecteds.append(ident)
            else:
                ejecteds.append(identify)
        return ejecteds

    @classmethod
    async def identifyEjection(cls, ctx: commands.Context, data):
        changed = False
        color = None
        imgDataPath = cls.defineUniquePath()
        imgFound = False
        name = None
        state = None
        if data is not None:
            if isinstance(data, str):
                name = data
                if len(data) == 1:
                    name = emoji.demojize(data)
                    code = '%04x' % ord(data)
                    uri = "https://raw.githubusercontent.com/twitter/twemoji/master/assets/svg/%s.svg" % code
                    request = requests.get(uri, stream=True)
                    if request.status_code == 200:
                        cairosvg.svg2png(url=uri, write_to=imgDataPath)
                        imgFound = True
                elif (data == "@everyone" or data == "@here"):
                    if isinstance(ctx.channel, TextChannel):
                        if data == "@here" and ctx.bot.intents.presences:
                            members = []
                            for member in await own_discord.fetchMembers(ctx.channel):

                                if member.status != Status.invisible and member.status != Status.offline:
                                    members.append(member)
                        else:
                            members = await own_discord.fetchMembers(ctx.channel)
                        return await cls.identifyEjections(ctx, members)
                    else:
                        name = data[1:]
                elif len(data.split("\n")) > 1:
                    return await cls.identifyEjections(ctx, data.split("\n"))
                else:
                    ids = []
                    nickNames = []
                    userNames = []
                    if isinstance(ctx.channel, DMChannel):
                        ids = [
                            ctx.bot.user.id,
                            ctx.author.id
                        ]
                        userNames = [
                            ctx.bot.user.name,
                            ctx.author.name
                        ]
                    else:
                        for member in ctx.channel.members:
                            if ctx.guild:
                                member = await ctx.guild.fetch_member(member.id)
                                nickNames.append(member.display_name)
                            ids.append(member.id)
                            userNames.append(member.name)
                    found = []
                    if ctx.guild:
                        for nickName in nickNames:
                            id = ids[nickNames.index(nickName)]
                            if nickName.lower().startswith(data.lower()) and not (id in found):
                                found.append(id)
                    for userName in userNames:
                        id = ids[userNames.index(userName)]
                        if userName.lower().startswith(data.lower()) and not (id in found):
                            found.append(id)
                    if len(found) == 1:
                        if ctx.guild:
                            data = await ctx.guild.fetch_member(found[0])
                        else:
                            data = ctx.bot.fetch_user(found)
            if isinstance(data, User):
                if ctx.bot.user.id == data.id:
                    data = ctx.author
                    changed = True
                if ctx.guild:
                    data = await ctx.guild.fetch_member(data.id)
                else:
                    name = data.name
                    if data.avatar is not None:
                        try:
                            await data.avatar.save(imgDataPath)
                            imgFound = True
                        except NotFound:
                            pass
            if isinstance(data, Member):
                name = data.display_name
                color = data.color
                try:
                    if data.guild_avatar is not None:
                        await data.guild_avatar.save(imgDataPath)
                        imgFound = True
                    elif data.avatar is not None:
                        await data.avatar.save(imgDataPath)
                        imgFound = True
                except NotFound:
                    pass
            if isinstance(data, Emoji):
                name = ":%s:" % data.name
                try:
                    await data.save(imgDataPath)
                    imgFound = True
                except NotFound:
                    pass
            if isinstance(data, Role):
                members = []
                if ctx.guild:
                    async for member in ctx.guild.fetch_members(limit=ctx.guild.max_members):
                        if data in member.roles and member.id != ctx.bot.user.id:
                            members.append(member)
                if len(members) > 0:
                    return await cls.identifyEjections(ctx, members)
                else:
                    name = "@%s" % data.name
                    color = data.color
            if isinstance(data, TextChannel):
                members = []
                for member in await own_discord.fetchMembers(data):
                    if member.id != ctx.bot.user.id:
                        members.append(member)
                if len(members) > 0:
                    return await cls.identifyEjections(ctx, members)
                else:
                    name = "#%s" % data.name
            if isinstance(data, Personna):
                name = data.name
                color = data.color
                if data.data is not None:
                    try:
                        await data.data.save(imgDataPath)
                        imgFound = True
                    except NotFound:
                        pass
                state = data.state
        if imgFound is False:
            attach = ctx.message.attachments
            if len(attach) > 0:
                target = attach[0]
                if name is None:
                    name = target.filename.split(".")[0]
                try:
                    await target.save(imgDataPath)
                    imgFound = True
                except NotFound:
                    pass
        return cls(name, color, imgDataPath if imgFound is True else None, changed, state)
