from discord import Color, Asset
import random
import copy


class StatePersonna:

    def __init__(self, name: str):
        self.name = name

    def __str__(self):
        return self.name


impostor = StatePersonna("Impostor")
crewmate = StatePersonna("Crewmate")
states = [
    impostor,
    crewmate
]


class DefendOption:

    maxCoef = 2
    increment = 0.25
    minCoef = 0.5

    def __init__(self, text: str, coef: float):
        if self.minCoef <= coef <= self.maxCoef and coef % self.increment == 0:
            self.text = text
            self.coef = coef
        else:
            raise AttributeError("Coef must range %f to %f and Coef must if multiple of %f." % (
                self.minCoef, self.maxCoef, self.increment))

    @property
    def impostorCoef(self):
        minMenacing = int(self.minCoef / self.increment)
        maxMenacing = int(self.maxCoef / self.increment)
        temp = []
        for nb in range(minMenacing, maxMenacing):
            temp.append(nb)
        temp.reverse()
        nbMenacing = int(self.coef / self.increment) - minMenacing
        return (temp[nbMenacing - 1]) * self.increment

    def __str__(self):
        return self.text

    @classmethod
    def ReallySafeOption(cls, text: str):
        return cls(text, cls.minCoef)

    @classmethod
    def SafeOption(cls, text: str):
        return cls(text, 0.75)

    @classmethod
    def NeutralOption(cls, text: str):
        return cls(text, 1)

    @classmethod
    def SuspectOption(cls, text: str):
        return cls(text, 1.5)

    @classmethod
    def ReallySuspectOption(cls, text: str):
        return cls(text, 1.75)

    @classmethod
    def TooSuspectOption(cls, text: str):
        return cls(text, cls.maxCoef)


class OwnColor:

    def __init__(self, name: str, color: Color):
        self.name = name
        self.color = color

    @classmethod
    def blue(cls):
        return cls("Blue", Color.dark_blue())

    @classmethod
    def lightBlue(cls):
        return cls("Light Blue", Color.blue())

    @classmethod
    def black(cls):
        return cls("Black", Color.from_rgb(0, 0, 0))

    @classmethod
    def white(cls):
        return cls("White", Color.from_rgb(255, 255, 255))

    @classmethod
    def brown(cls):
        return cls("Brown", Color.from_rgb(90, 57, 41))

    @classmethod
    def green(cls):
        return cls("Green", Color.dark_green())

    @classmethod
    def lightGreen(cls):
        return cls("Light Green", Color.green())

    @classmethod
    def yellow(cls):
        return cls("Yellow", Color.gold())

    @classmethod
    def orange(cls):
        return cls("Orange", Color.orange())

    @classmethod
    def red(cls):
        return cls("Red", Color.red())

    @classmethod
    def pink(cls):
        return cls("Pink", Color.from_rgb(223, 63, 168))

    @classmethod
    def purple(cls):
        return cls("Purple", Color.purple())


colors = [
    OwnColor.black(),
    OwnColor.blue(),
    OwnColor.brown(),
    OwnColor.green(),
    OwnColor.lightBlue(),
    OwnColor.lightGreen(),
    OwnColor.orange(),
    OwnColor.pink(),
    OwnColor.purple(),
    OwnColor.red(),
    OwnColor.white(),
    OwnColor.yellow()
]

cinema = DefendOption.TooSuspectOption(
    "It's not me. I have an alibi. I was at the cinema.")
imNotImpostor = DefendOption.ReallySuspectOption("I'm not the Impostor !")
cardAdmin = DefendOption.NeutralOption(
    "I tried to pass this f*uck*ng card in Admin !")
wireElectrical = DefendOption.NeutralOption(
    "I'm going to fix wires in Electrical.")
simonReactor = DefendOption.NeutralOption(
    "I tried to resolve Simon in Reactor !")
accusator = DefendOption.SuspectOption("I saw someone vent in Reactor !")
killView = DefendOption.SuspectOption("I saw someone kill in storage")
trueStalking = DefendOption.NeutralOption(
    "I saw someone enter in storage before doors have closed.")
falseStalking = DefendOption.SuspectOption(
    "I saw someone go away after electrical cut-off !")
visualTaskMed = DefendOption.ReallySafeOption(
    "I've been scanned in Medbay and someone can confirm !")
visualTaskWeapon = DefendOption.SafeOption(
    "I was trying to finish the task at Weapons Room and you can come and check after this meeting !")
visualTaskShield = DefendOption.NeutralOption(
    "I've reactivated the shields, but no one saw me do it...")


class Personna:

    defends = {
        impostor: [
            cinema,
            imNotImpostor,
            accusator,
            falseStalking,
            wireElectrical,
            simonReactor,
            cardAdmin,
            visualTaskShield
        ],
        crewmate: [
            visualTaskMed,
            visualTaskShield,
            visualTaskWeapon,
            cardAdmin,
            simonReactor,
            wireElectrical,
            trueStalking,
            accusator,
            killView
        ]
    }

    def __init__(self, state: StatePersonna, color: OwnColor, data: Asset = None):
        self.state = state
        self.ownColor = color
        self.color = self.ownColor.color
        self.name = self.ownColor.name
        self.data = data

    @property
    def defend(self):
        base = self.defends[self.state]
        return base[random.randint(0, len(base) - 1)]

    def vote(self, defendsArg: list):
        tot = 0
        for defend in defendsArg:
            tot += defend.coef if self.state == crewmate else defend.impostorCoef
        value = random.uniform(0, tot + 1)
        act = 0
        for defend in defendsArg:
            if act <= value < act + defend.coef:
                return defendsArg.index(defend)
            act += defend.coef
        return None


class Crew:

    maximumMembers = 9
    minimumMembers = 3
    upImpostor = 5

    def __init__(self, you: Personna, nb: int = None):
        self.you = you
        if nb is not None and (nb < self.minimumMembers or nb > self.maximumMembers):
            raise AttributeError("Crew is composed by %i to %i members" % (
                self.minimumMembers, self.maximumMembers))
        self.nb = nb if nb is not None else random.randint(
            self.minimumMembers, self.maximumMembers)
        self.impostorNb = 1 if self.nb < self.upImpostor else 2
        self.impostorNb -= 1 if you.state == impostor else 0
        self.members = [you]
        self.colorsSelected = copy.copy(colors)
        for nb in range(len(colors) - self.nb):
            self.colorsSelected.pop(random.randint(
                0, len(self.colorsSelected) - 1))
        tempColors = copy.copy(self.colorsSelected)
        for temp in range(self.impostorNb):
            self.members.append(Personna(impostor, tempColors.pop(
                random.randint(0, len(tempColors) - 1))))
        for temp in range(self.nb - self.impostorNb):
            self.members.append(Personna(crewmate, tempColors.pop(
                random.randint(0, len(tempColors) - 1))))
        random.shuffle(self.members)
        self.killed = []

    def copy(self):
        copySelf = copy.copy(self)
        copySelf.members = copy.copy(self.members)
        copySelf.killed = copy.copy(self.killed)
        return copySelf

    @property
    def impostorAlive(self):
        for member in self.members:
            if member.state == impostor:
                return True
        return False

    @property
    def nbImpostorAlive(self):
        nb = 0
        for member in self.members:
            if member.state == impostor:
                nb += 1
        return nb

    @property
    def nbCrewmatesAlive(self):
        nb = 0
        for member in self.members:
            if member.state == crewmate:
                nb += 1
        return nb

    @property
    def nbAlive(self):
        return len(self.members)

    def kill(self, member: Personna):
        if member is not None and member in self.members:
            self.killed.append(member)
            self.members.remove(member)

    def showDead(self):
        if len(self.killed) > 0:
            msg = "Dead:\n"
            for personna in self.killed:
                msg = "%s\t%s%s - %s\n" % (
                    msg,
                    personna.name,
                    " (You)" if self.you == personna else "",
                    personna.state
                )
            return msg
        return ""

    def showMember(self, personna: Personna, omniscient: bool = False):
        return "%s%s%s" % (
            personna.name,
            " (You)" if self.you == personna else "",
            (" - %s" % personna.state) if (personna.state ==
                                           impostor == self.you.state or omniscient is True) else ""
        )

    def showCrew(self, omniscient: bool = False, withoutDeath: bool = False):
        msg = "Crew:\n"
        for personna in self.members:
            msg = "%s\t%s\n" % (msg, self.showMember(personna, omniscient))
        return "%s\n%s" % (msg, self.showDead() if withoutDeath is False else "")

    def showCrewWithDefend(self, defends: list, indexDefend: int = None, omniscient: bool = False, withoutDeath: bool = False):
        if indexDefend is None:
            indexDefend = len(self.members) - 1
        msg = "Crew:\n"
        for personna in self.members:
            currentIndex = self.members.index(personna)
            msg = "%s%i - %s" % (msg, currentIndex + 1,
                                 self.showMember(personna, omniscient))
            if currentIndex < indexDefend and len(defends) != 0:
                msg = "%s :\n> `%s`" % (msg, defends[currentIndex])
            msg = "%s\n" % msg
        return "%s\n%s" % (msg, self.showDead() if withoutDeath is False else "")
