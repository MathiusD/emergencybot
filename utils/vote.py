from discord import Color, Message
from discord.ext import commands
from own import own_discord
from .ejection import Ejection
from .crew import Crew, Personna, states, OwnColor, impostor, crewmate
from .events import PhasedEvent, State, Phase, Arg, ArgCond, BaseEvent, PushCancelableTimeEvent, PushEvent, BaseEvent, PushLimitedTimeEvent
import random
import asyncio
import os
import copy


class ArgCondVote(ArgCond):

    name = "crew"

    def check(self, event: BaseEvent):
        if hasattr(event, self.name):
            crewValue = getattr(event, self.name)
            if crewValue.impostorAlive == False:
                return True
            if crewValue.nbImpostorAlive == crewValue.nbCrewmatesAlive:
                return True
        return False


crewArg = Arg("crew", True, True)
crewArgs = [crewArg]


class PresentationEvent(PushCancelableTimeEvent):
    message = "AmongUs Discord"
    description = "Welcome in AmongUs Discord Event"
    pushEmojis = ["🆗"]
    onlyRequestUser = True

    def _explanation(self):
        return "You are %s ! (Push 🆗 for launch Game)\n\n%s" % (self.crew.you.state, self.crew.showCrew())

    async def end(self):
        self.finish = True
        if await self.state != State.SUCESS:
            await self.updateEnd()


class DiscussionEvent(PushCancelableTimeEvent):

    message = "Discution Time !"
    description = "There is an impostor among us, we must find him and eject him!"
    pushEmojis = ["✅"]
    onlyRequestUser = True

    def _explanation(self):
        return "Discution Time ! (Push ✅ for passed to Votting Time)\n\n%s" % self.crew.showCrew()

    async def end(self):
        self.finish = True
        if await self.state != State.SUCESS:
            await self.updateEnd()


class VoteEvent(PushLimitedTimeEvent):

    message = "Time to vote !"
    description = "Now vote for eject anyone !"
    timeout = 60
    pushEmojis = [
        "1️⃣",
        "2️⃣",
        "3️⃣",
        "4️⃣",
        "5️⃣",
        "6️⃣",
        "7️⃣",
        "8️⃣",
        "9️⃣",
        "🔟"
    ]
    onlyRequestUser = True

    def __init__(self, ctx: commands.Context, msg: Message = None):
        self.defends = []
        super().__init__(ctx, msg)

    async def launch(self):
        for member in self.crew.members:
            self.defends.append(member.defend)
        self.votes = []
        for member in self.crew.members:
            index = self.crew.members.index(member)
            if member != self.crew.you:
                tempDefends = copy.copy(self.defends)
                tempDefends.pop(index)
                tempVote = member.vote(tempDefends)
                self.votes.append(
                    tempVote + 1 if tempVote is not None and tempVote >= index else tempVote)
            else:
                self.youIndex = index
                self.votes.append(None)
        await super().launch()

    async def addEmojis(self):
        if self.crew.you in self.crew.members:
            for nb in range(self.crew.nbAlive):
                await self.msg.add_reaction(self.pushEmojis[nb])

    @property
    async def state(self):
        if self.crew.you in self.crew.members:
            count = await self._comptEmoji()
            confirmed = []
            for comptKey in count:
                compt = count[comptKey]
                if compt == 1:
                    confirmed.append(comptKey)
            if self.isTimeout is False:
                if len(confirmed) == 1:
                    self.votes[self.youIndex] = self.pushEmojis.index(
                        confirmed[0])
                else:
                    return State.IN_PROGRESS
        return State.SUCESS

    def _explanation(self):
        msg = "Time to vote !(Vote for anyone) %s\n\n" % (self.timeStr)
        msg = "%s%s" % (msg, self.crew.showCrewWithDefend(
            self.defends, self.timeElapsed / 5))
        return msg

    async def end(self):
        self.finish = True
        self.maxVote = 0
        self.resultsVote = {}
        self.ejectedMembers = []
        compt = 0
        for vote in self.votes:
            if vote is not None:
                personna = self.crew.members[vote]
                accusator = self.crew.members[compt]
                if not personna in self.resultsVote:
                    self.resultsVote[personna] = {
                        "compt": 1,
                        "accusators": [
                            accusator
                        ]
                    }
                else:
                    self.resultsVote[personna]["compt"] += 1
                    self.resultsVote[personna]["accusators"].append(accusator)
                if self.resultsVote[personna]["compt"] == self.maxVote:
                    self.ejectedMembers.append(personna)
                elif self.resultsVote[personna]["compt"] > self.maxVote:
                    self.ejectedMembers.clear()
                    self.ejectedMembers = [personna]
                    self.maxVote = self.resultsVote[personna]["compt"]
            compt += 1


class EjectionEvent(PushCancelableTimeEvent):

    message = "Ejection"
    description = None
    pushEmojis = ["➕"]
    onlyRequestUser = True

    async def launch(self):
        self.oldCrew = self.crew.copy()
        self.memberEjected = self.crew.members[self.crew.members.index(
            self.ejectedMembers[0])] if len(self.ejectedMembers) == 1 else None
        self.crew.kill(self.memberEjected)
        self.updated = False
        self.checkState = False
        self.ejected = await Ejection.identifyEjection(self.ctx, self.memberEjected)
        self.description = self.ejected.createMsg()
        await super().launch()

    async def _state(self):
        while self.updated is True:
            await asyncio.sleep(1)
        self.checkState = True
        state = await super()._state()
        self.checkState = False
        return state

    async def update(self, embed: list = None):
        while self.checkState is True:
            await asyncio.sleep(1)
        self.updated = True
        path = self.ejected.createImg()
        newMsg = await own_discord.embed_send(self.ctx, self.embed if embed is None else embed, react_unlocked=True, file=self.ejected.discordFile(path))
        os.remove(path)
        tempMsg = self.msg
        self.msg = newMsg
        for emoji in self.pushEmojis:
            await self.msg.add_reaction(emoji)
        await tempMsg.delete()
        self.updated = False

    def _explanation(self):
        crew = ""
        compt = 0
        for line in self.oldCrew.showCrew(withoutDeath=True).split("\n"):
            if line != "Crew:" and line != "":
                personna = self.oldCrew.members[compt]
                if personna in self.resultsVote.keys():
                    line = "%s\n\tVoted by :\n" % line
                    for accusator in self.resultsVote[personna]["accusators"]:
                        line = "%s> %s\n" % (line, accusator.name)
                    line = line[:-1]
                compt += 1
            crew = "%s%s\n" % (crew, line)
        return "Press ➕ for %s.\n%s\n%s" % ("pray respect" if self.memberEjected is not None else "skip", crew, self.oldCrew.showDead())

    async def updateEnd(self):
        state = await self.state
        embed = own_discord.embed_create(self.content, name="%s (%s)" % (
            self.message, state.name), colour=self.color)
        await BaseEvent.update(self, embed)

    async def end(self):
        self.finish = True
        if await self.state != State.SUCESS:
            await self.updateEnd()


class EndGameEvent(BaseEvent):
    message = "End of Game"
    description = "End of Game"

    def _explanation(self):
        return "%s are winners !\n%s" % ("Crewmates" if self.crew.impostorAlive == False else "Impostor(s)", self.crew.showCrew(True))

    async def _state(self):
        if self.crew.impostorAlive == False and self.crew.you.state == crewmate:
            return State.SUCESS
        if self.crew.nbImpostorAlive == self.crew.nbCrewmatesAlive and self.crew.you.state == impostor:
            return State.SUCESS
        return State.FAILED


class AllVotePhaseEvent(PhasedEvent):

    message = "Base All Vote Event"
    phases = [
        Phase(DiscussionEvent, crewArgs),
        Phase(
            VoteEvent,
            [
                crewArg,
                Arg("ejectedMembers", False, True),
                Arg("resultsVote", False, True)
            ]
        ),
        Phase(
            EjectionEvent,
            [
                crewArg,
                Arg("ejectedMembers", True, False),
                Arg("resultsVote", True, False)
            ])
    ]


class GameEvent(PhasedEvent):

    message = "Base Game Event"
    phases = [
        Phase(PresentationEvent, crewArgs),
        Phase(
            AllVotePhaseEvent,
            crewArgs,
            ArgCondVote()
        ),
        Phase(EndGameEvent, crewArgs)
    ]

    async def launch(self):
        if self.ctx.author.guild_avatar is not None:
            dataImg = self.ctx.author.guild_avatar
        elif self.ctx.author.avatar is not None:
            dataImg = self.ctx.author.avatar
        else:
            dataImg = None
        state = states[random.randint(0, len(states) - 1)]
        you = Personna(
            state,
            OwnColor(
                self.ctx.author.name,
                Color.from_rgb(random.randint(0, 255), random.randint(
                    0, 255), random.randint(0, 255))
            ),
            dataImg
        )
        if self.ctx.guild:
            data = await self.ctx.guild.fetch_member(self.ctx.author.id)
            if data:
                you = Personna(
                    state,
                    OwnColor(data.display_name, data.color),
                    dataImg
                )
        self.crew = Crew(you)
        await super().launch()
