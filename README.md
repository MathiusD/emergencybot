# Emergency Bot

![pipeline](https://gitlab.com/MathiusD/emergencybot/badges/master/pipeline.svg)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/4f896af36e5c46e4951ee99df2ac83c9)](https://www.codacy.com/manual/MathiusD/emergencybot?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=MathiusD/emergencybot&amp;utm_campaign=Badge_Grade)

## Auteur

Féry Mathieu (aka Mathius)
